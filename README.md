# ci-docker-image-cache

A simple system to allow caching docker images used during the CI/CD pipelines.

## How does it work?

1. A SHA256 hash of the Dockerfile is used as part of the image name
1. If the docker image can be pulled, it is used
1. If the docker image cannot be pulled, it is built and pushed
1. The cached images are removed on a configurable schedule

## Dependencies

- Bash
- Docker
- sha256sum

## Usage

### Load or build an image

Called to build and cache an image, or load an existing cached copy.

```shell
Syntax: docker-cache -c CONFIG_FILE IMAGE_NAME

  -c CONFIG_FILE  Configuration file for docker-cache
  IMAGE_NAME      Name of an image to load or build. Must correspond
                  to a dockerfile in the directory configured by
                  the 'dockerfiles' configuration key and matching
                  the format 'Dockerfile-IMAGE_NAME'.
```

### Cleanup cached images

This command can be run from a scheduled pipeline, or on all pipelines.

```shell
Syntax: docker-cache-cleaner -c CONFIG_FILE
```

### Configuration

The configuration file is key value pairs in the same format
as setting shell variables.

| Key                 | Description |
| :------------------ | :---------- |
| docker_username     | Docker username (e.g. `gitlab-ci-token`) |
| docker_password_var | Docker password environment variable (e.g. `CI_JOB_TOKEN`) |
| docker_registry     | Docker registry (e.g. `registry.gitlab.com/mikeeddington/ci-docker-image-cache`) |
| dockerfiles         | Directory containing dockerfiles to cache |
| image_prefix        | [Optional] Image name prefix. Defaults to `dcache-`. |
| cache_length        | [Optional] How long a cached image is kept before being rebuilt. Default to `15 days`. |

#### `dockerfiles` folder structure

A specific naming convention is required for files in this folder: `Dockerfile-NAME`

For example, given the following docker files:

- `Dockerfile-image1`
- `Dockerfile-image2`
- `Dockerfile-image3`

The following command lines are valid:

- `docker-cache -c .docker-cache.conf image1`
- `docker-cache -c .docker-cache.conf image2`
- `docker-cache -c .docker-cache.conf image3`

#### Example

_Minimal:_

```shell
docker_username=gitlab-ci-token
docker_password_var=CI_JOB_TOKEN
docker_registry_var=CI_REGISTRY
dockerfiles=test/dockerfiles
```

_Full:_

```shell
docker_username=gitlab-ci-token
docker_password_var=CI_JOB_TOKEN
docker_registry_var=CI_REGISTRY
dockerfiles=test/dockerfiles
image_prefix=abcxyz-
cache_length="30 days"
```
