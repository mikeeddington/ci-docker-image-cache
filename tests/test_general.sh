#!/usr/bin/env bash

################################################################
## Figure out our current folder

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

################################################################
## Globals

TEST_SUITE=test_general
DOCKER_CACHE=$DIR/../docker-cache
DOCKER_CACHE_CLEANER=$DIR/../docker-cache-cleaner
DOCKERFILES=$DIR/dockerfiles
TMP=/tmp
CONFIG_FILE=$TMP/$TEST_SUITE.conf

################################################################
## Helper functions

create_config() {
    echo "docker_username=gitlab-ci-token" > $CONFIG_FILE
    echo "docker_password_var=CI_JOB_TOKEN" >> $CONFIG_FILE
    echo "docker_registry=$CI_REGISTRY/$CI_PROJECT_PATH" >> $CONFIG_FILE
    echo "dockerfiles=$DOCKERFILES" >> $CONFIG_FILE
    echo "image_prefix=$TEST_SUITE-" >> $CONFIG_FILE
    echo "cache_length=\"1 days\"" >> $CONFIG_FILE
}

rm_images_by_prefix() {
    local prefix=$1

    docker rmi -f $(docker images --filter=reference=${TEST_SUITE}* -q) &> /dev/null
}

################################################################
## Tests

test_build_then_cached() {

    create_config
    rm_images_by_prefix $TEST_SUITE
    local out="$(mktemp)"

    $DOCKER_CACHE -c $CONFIG_FILE image1 &> $out
    rc=$?
    local output=$(cat $out)

    assert_equals "0" "$rc" "Expected docker-cache exit code of 0 when image doesn't exist yet.: $output"
    assert_fail "grep -q \"$image1 not found, building and pushing\" $out" "Expected to find \"$image1 not found, building and pushing\" in output: $output"

    $DOCKER_CACHE -c $CONFIG_FILE image1 &> $out
    rc=$?
    local output=$(cat $out)

    assert_equals "0" "$rc" "Expected docker-cache exit code of 0 when image does exist.: $output"
    assert "grep -q \"$image1 found in cache\" $out" "Expected to find \"$image1 found in cache\" in output.: $output"

    rm_images_by_prefix $TEST_SUITE
    rm -f $out
}
